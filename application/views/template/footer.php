                    </div>
                    </div>

                    <footer class="footer">
                        <div class="container-fluid">
                            <nav class="pull-left">
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Company</a></li>
                                    <li><a href="#">About us</a></li>
                                </ul>
                            </nav>
                            <p class="copyright pull-right">
                                &copy;
                                <script>
                                    document.write(new Date().getFullYear())
                                </script>
                                <a href="#">Tambang Nikel Indonesia Company</a>
                            </p>
                        </div>
                    </footer>
                    </div>
                    </div>


                    <!--	Core JS Files	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>"></script>
                    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
                    <script type="text/javascript" src="<?php echo base_url('assets/js/material.min.js') ?>"></script>
                    <script type="text/javascript" src="<?php echo base_url('assets/js/perfect-scrollbar.jquery.min.js') ?>"></script>

                    <!--	Forms Validations Plugin 	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js') ?>"></script>
                    <!--	Plugin for Date Time Picker and Full Calendar Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/moment.min.js') ?>"></script>
                    <!--	Plugin for the Wizard	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.bootstrap-wizard.js') ?>"></script>
                    <!--	Notifications Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-notify.js') ?>"></script>
                    <!--	DateTimePicker Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js') ?>"></script>
                    <!--	Sliders Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/nouislider.min.js') ?>"></script>
                    <!--	Select Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.select-bootstrap.js') ?>"></script>
                    <!--	DataTables Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.datatables.js') ?>"></script>
                    <!--	Sweet Alert 2 Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/sweetalert2.js') ?>"></script>
                    <!--	Fileupload Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/jasny-bootstrap.min.js') ?>"></script>
                    <!--	Fileupload Plugin	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.tagsinput.js') ?>"></script>
                    <!--	Material Dashboard	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/material-dashboard.js') ?>"></script>
                    <!--	Material Dashboard Demo	-->
                    <script type="text/javascript" src="<?php echo base_url('assets/js/demo.js') ?>"></script>
                    <script type="text/javascript" src="<?php echo base_url('assets/js/template.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            demo.initDataTables();
                            demo.initMaterialWizard();
                            demo.initFormExtendedDatetimepickers();
                        })
                    </script>
                    </body>
                    <footer>

                    </footer>

                    </html>