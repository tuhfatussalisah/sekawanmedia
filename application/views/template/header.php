<?php $user = $_SESSION['sess_user'] ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/apple-icon.png') ?>" />
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.png') ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Tambang Nikel Indonesia Company</title>

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url('assets/css/material-dashboard.css') ?>" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url('assets/css/demo.css') ?>" rel="stylesheet" />
    <!-- <link href="<?php //echo base_url('assets/css/template.css') 
                        ?>" rel="stylesheet" /> -->
    <!--     Fonts and icons     -->
    <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/google-roboto-300-700.css') ?>" rel="stylesheet" />

    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.1.1.min.js') ?>"></script>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="red" data-background-color="black" data-image="<?php echo base_url('assets/img/sidebar-1.jpg') ?>">
            <div class="logo">
                <a href="http://www.creative-tim.com/" class="simple-text">
                    Tambang Nikel
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="http://www.creative-tim.com/" class="simple-text">
                    Ct
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="<?php echo base_url('assets/img/default-avatar.png ') ?>">
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <?php echo $user['user_name'] ?>
                            <b class="caret"></b>
                        </a>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="#">My Profile</a>
                                </li>
                                <li>
                                    <a href="#">Edit Profile</a>
                                </li>
                                <li>
                                    <a href="#">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="nav">
                    <li class="<?php echo $this->uri->segment(2) == 'dashboard' ? 'active' : '' ?>">
                        <a href="<?php echo base_url($user['user_level'] . '/dashboard') ?>">
                            <i class="material-icons">home</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <?php if ($user['user_level'] == 'admin') : ?>
                        <li class="<?php echo $this->uri->segment(2) == 'kendaraan' ? 'active' : '' ?>">
                            <a href="<?php echo base_url($user['user_level'] . '/kendaraan') ?>">
                                <i class="material-icons">dashboard</i>
                                <p>Kendaraan</p>
                            </a>
                        </li>
                    <?php endif ?>
                    <li>
                        <a href="<?php echo base_url('logout') ?>">
                            <i class="material-icons">Logout</i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <div class="content">
                <div class="container-fluid">