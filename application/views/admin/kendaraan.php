<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="red">
                <h4 class="card-title">Daftar Kendaraan Angkutan Orang dan Barang</h4>
                <p class="card-category">Tambang Nikel Indonesia Company</p>
            </div>
            <div class="card-content">
                <a data-toggle="modal" data-target="#modal" data-action="create" data-form="form_kendaraan" data-table="kendaraan" class="btn btn-danger pop-up">Add Data</a>
                <div class="table-responsive">
                    <table id="datatables" class="table table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead class="text-primary">
                            <th class="text-center">No.</th>
                            <th class="text-left">Nama Kendaraan</th>
                            <th class="text-left">Tipe Kendaraan</th>
                            <th class="text-center">Option</th>
                        </thead>
                        <tbody>
                            <?php foreach ($kendaraan as $key => $value) : ?>
                                <tr>
                                    <td class="text-center"><?php echo $key + 1 ?></td>
                                    <td class="text-left"><?php echo $value['kendaraan_name'] ?></td>
                                    <td class="text-left"><?php echo $value['kendaraan_type'] ?></td>
                                    <td class="text-center">
                                        <a data-toggle="modal" data-target="#modal" data-action="update" data-form="form_kendaraan" data-table="kendaraan" class="btn btn-xs btn-warning btn-update pop-up" idnya="<?php echo $value['kendaraan_id'] ?>">
                                            <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a href="<?php echo base_url('admin/kendaraan/delete/' . $value['kendaraan_id']) ?>" class="btn btn-xs btn-danger btn-delete">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal">
    <div class="modal-dialog modal-sm modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
                <h4 class="modal-title"></h4>
            </div>
            <form method="post" id="form_kendaraan">
                <div class="modal-body">
                    <div class="instruction">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama Kendaraan</label>
                                    <input type="text" class="form-control" name="kendaraan_name" val="">
                                    <div class="invalid-feedback text-danger"></div>
                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">Tipe Kendaraan</label>
                                    <div class="invalid-feedback text-danger"></div>
                                    <select class="form-control" data-style="select-with-transition" name="kendaraan_type">
                                        <option disabled selected>Pilih Tipe Kendaraan</option>
                                        <option>angkutan orang</option>
                                        <option>angkutan barang</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger action">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var pesan = '<?php echo $this->session->flashdata('pesan') ?>';
        if (pesan != '') {
            notif(pesan);
        }

        $(document).on('click', '.create', function(e) { //e mengambil event saat formulir disave
            e.preventDefault(); //mencegah semua event ketika mengirim formulir
            formulir('kendaraan', 'create', 'form_kendaraan'); //proses ketika klik create menjalankan formulir dan menjalankan kendaraan dan create
        });
        $(document).on('click', '.btn-update', function() {
            var id = $(this).attr('idnya');
            $('#form_kendaraan input[name=kendaraan_id]').val(id);
            // alert(id);

            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('admin/kendaraan/detail') ?>',
                data: 'kendaraan_id=' + id,
                dataType: 'json',
                success: function(result) {
                    // console.log(result)
                    $('#form_kendaraan input[name=kendaraan_name]').val(result.kendaraan_name);
                    $('#form_kendaraan textarea[name=kendaraan_type]').val(result.kendaraan_type);

                    $('#form_kendaraan .label-floating').removeClass('is-empty');
                }
            })
        })

        $(document).on('click', '.update', function(e) {
            e.preventDefault(); //mencegah semua event ketika mengirim formulir
            formulir('kendaraan', 'update', 'form_kendaraan'); //proses ketika klik create menjalankan formulir dan menjalankan kendaraan dan create
        });

        $(document).on('click', '.btn-delete', function(e) {
            e.preventDefault();

            var href = $(this).attr('href'); //href untuk mengambil href / atribut dari .btn-delete yg diklik
            del(href); //memberikan href ke fungsi del di js
        })
    })
</script>