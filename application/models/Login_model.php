<?php

class Login_model extends CI_Model
{
    function validasi($inputan_form)
    {
        $username = $inputan_form['username'];
        $password = sha1($inputan_form['password']);

        $this->db->where('user_username', $username);
        $this->db->where('user_password', $password);
        $data_user = $this->db->get('user')->row_array();

        if (empty($data_user))
            return 'gagal';

        $this->session->set_userdata('sess_user', $data_user);
        return 'sukses';
    }
}
