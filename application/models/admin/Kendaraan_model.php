<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kendaraan_model extends CI_Model
{
    public function getAll()
    {
        $ambil = $this->db->get('kendaraan');
        $array = $ambil->result_array();
        return $array;
    }

    public function create($formulir)
    {
        $this->db->insert('kendaraan', $formulir);
    }

    public function get_detail($id)
    {
        $this->db->where('kendaraan_id', $id);
        $ambil = $this->db->get('kendaraan');
        $array = $ambil->row_array();
        return $array;
    }

    public function update($formulir)
    {
        $this->db->where('kendaraan_id', $formulir['kendaraan_id']);
        $this->db->update('kendaraan', $formulir);
    }

    function delete($id)
    {
        $this->db->where('kendaraan_id', $id)->delete('kendaraan');
    }
}
