<?php

class login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }

    public function index()
    {
        if ($this->session->userdata('sess_user')) {
            $level = $this->session->userdata('sess_user')['user_level'];
            redirect($level . '/dashboard');
        }

        $data_formulir = $this->input->post();


        if ($data_formulir) {
            $validasi = $this->login_model->validasi($data_formulir);

            if ($validasi == 'gagal')
                redirect('', 'refresh');

            $data_user = $this->session->userdata('sess_user');
            redirect($data_user['user_level'] . '/dashboard');
        }
        $this->load->view('template/login');
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect('', 'refresh');
    }
}
