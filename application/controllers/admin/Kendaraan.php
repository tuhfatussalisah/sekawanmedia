<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kendaraan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (level('admin'))
            redirect('');

        $this->load->model('admin/kendaraan_model');
    }

    public function index()
    {
        $data['kendaraan'] = $this->kendaraan_model->getAll();

        $this->load->view('template/header');
        $this->load->view('admin/kendaraan', $data);
        $this->load->view('template/footer');
    }

    public function create()
    {
        $this->form_validation->set_rules('kendaraan_name', 'Name', 'required|is_unique[kendaraan.kendaraan_name]');
        $this->form_validation->set_rules('kendaraan_type', 'Type', 'required');

        if ($this->form_validation->run()) {
            $formulir = $this->input->post();
            $this->kendaraan_model->create($formulir);

            $this->session->set_flashdata('pesan', 'Data berhasil ditambahkan!');
            $pesan = ['status' => 'success'];
        } else {
            $validasi = $this->form_validation->error_array();
            $pesan = ['status' => 'error', 'validation' => $validasi];
        }

        echo json_encode($pesan);
    }

    public function detail()
    {
        $id = $this->input->post('kendaraan_id');
        $data = $this->kendaraan_model->get_detail($id);

        echo json_encode($data);
    }

    public function update()
    {
        $formulir = $this->input->post();
        $data = $this->kendaraan_model->get_detail($formulir['kendaraan_id']);

        $nameUnique = $formulir['kendaraan_name'] == $data['kendaraan_name'] ? '' : '|is_unique[kendaraan.kendaraan_name]';

        $this->form_validation->set_rules('kendaraan_name', 'Name', 'required' . $nameUnique);
        $this->form_validation->set_rules('kendaraan_type', 'Type', 'required');

        if ($this->form_validation->run()) {
            $this->kendaraan_model->update($formulir);

            $this->session->set_flashdata('pesan', 'Data berhasil diubah!');
            $pesan = ['status' => 'success'];
        } else {
            $validasi = $this->form_validation->error_array();
            $pesan = ['status' => 'error', 'validation' => $validasi];
        }

        echo json_encode($pesan);
    }

    public function delete($id)
    {
        $this->kendaraan_model->delete($id);

        $this->session->set_flashdata('pesan', 'Data berhasil dihapus!');
        redirect('admin/kendaraan');
    }
}
