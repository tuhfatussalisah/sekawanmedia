var url = window.location.href.split("/");

//http: / / localhost / hmif / admin / periode
//http: / / localhost / hmif / staff / periode
// 0     1      2        3       4         5

var base_url = window.location.protocol+'//'+window.location.hostname+'/'+url[3]+'/'+url[4]+'/';

function notif(pesan){
    swal({
        type: pesan.toLowerCase().includes('berhasil') ? 'success' : 'error',
        title: 'Informasi',
        text: pesan,
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success',
        timer: 2000
    });
}

//modifikasi modal atau konfigurasi
$('.pop-up').on('click', function() {
    var action = $(this).data('action');
    // var table = $(this).data('table');
    var title = action.charAt(0).toUpperCase() + action.slice(1) + ' Data';

    $('.modal-title').html(title); //merubah title menjadi create atau update

    var form = $(this).data('form');
    $('#'+form+' .invalid-feedback').html('');
    $('#'+form+' .form-group').removeClass('has-error');
    $('#'+form+' input, #'+form+' select, #'+form+' textarea').removeClass('valid error');

    var table = $(this).data('table');

    if (action == 'update'){
        $('#'+form+' input[name='+table+'_id]').remove();
        $('#'+form).prepend("<input type='hidden' name='"+table+"_id'>"); //menambahkan inputan id (hidden)

        $('#'+form+' .action').removeClass('create');
		$('#'+form+' .action').addClass('update')
    } else { //selain updata which is cuma ada create
        $('#'+form+' input[name='+table+'_id]').remove();

        $('#'+form+' .action').removeClass('update');
		$('#'+form+' .action').addClass('create');

        $('#'+form).trigger('reset'); //untuk reset formulir
    }
});

function validation(form, dataValidasi){
    //mengambil semua name yang ada di formulir
    var name = [];
    //mengambil semua tag input dan tag select yang dibungkus dengan id form
    // $('#form_periode input, #form_periode select')
    $('#'+form+' input, #'+form+' select, #'+form+' textarea').each(function() {
        //ambil isi dari atribut name
        nameForm = $(this).attr('name');

        //cek isi atribut sudah ada apa tidak pada array nama diatas
        if(!name.includes(nameForm)){
            //kalau tidak ada masukan isi atribut name tadi ke array
            name.push(nameForm);
        }
    });

    name.forEach(function(nameTag){
        if (dataValidasi[nameTag]) {
            // $("#formPeriode [name='tanggal']")
            $("#"+form+" [name='"+nameTag+"']").parents('.form-group').addClass('has-error');
            $("#"+form+" [name='"+nameTag+"']").parents('.form-group').children('.invalid-feedback').html(dataValidasi[nameTag]);

            $("#"+form+" [name='"+nameTag+"']").addClass('error');
            $("#"+form+" [name='"+nameTag+"']").removeClass('valid'); //valid membuat kelas menjadi warna hijau
        } else {
            $("#"+form+" [name='"+nameTag+"']").parents('.form-group').removeClass('has-error');
            $("#"+form+" [name='"+nameTag+"']").parents('.form-group').children('.invalid-feedback').html('');

            $("#"+form+" [name='"+nameTag+"']").removeClass('error');
            $("#"+form+" [name='"+nameTag+"']").addClass('valid'); //valid membuat kelas menjadi warna hijau
        }
    });  

    // console.log(name)
    // console.log(dataValidasi)
}

//fungsi mengirimkan data formulir berdasarkan controller dan method tujuan
//                  periode   create  form_periode
function formulir(controller, method, form='form'){
    $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: base_url+controller+'/'+method,
        data: new FormData($('#'+form)[0]), //mengambil seluruh inputan yg ada di formulir
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'json',
        success: function(result)
        {
            // location.reload(); //mereload setelah proses selesai
            // console.log(result);
            if (result.status == 'success'){
                location.reload();
            } else {
                validation(form, result.validation);
            }
        }
    })
}

function del(href){
    swal({
        title: 'Apakah Anda Yakin?',
        text: 'Data akan dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        buttonsStyling: false
    }).then(function() { document.location.href = href; })
        // http://localhost/hmif/admin/periode/delete/1
}